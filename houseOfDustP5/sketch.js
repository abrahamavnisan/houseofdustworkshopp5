function setup() {

	// create a canvas to fill the entire browser
	canvas = createCanvas(window.innerWidth, window.innerHeight);
}
function draw() {
  // put drawing code here
  if (mouseIsPressed) {
    fill(0);
  } else {
    fill(255);
  }
  ellipse(mouseX, mouseY, 80, 80);
}