# House of Dust by Alison Knowles

## p5.js implementation for a workshop at [U R Here: A Festival of Digital Language Art](https://digitalstudies.camden.rutgers.edu/urhere/)

This repository contains a [p5.js](https://p5js.org/) implementation of a poetry generator inspired by [Alison Knowles' House of Dust,](http://blog.calarts.edu/2009/09/10/alison-knowles-james-tenney-and-the-house-of-dust-at-calarts/) one of the first computer-generated poems. It was created on the occasion of my leading a workshop as part of U R Here: A Festival of Digital Language Art, held at Rutgers University Camden on November 3 + 4, 2017.

There are three branches:

  * "master" contains the "starter kit" for workshop participants
  * "text" contains a finished, text-only implementation
  * "speech" contains a finished implementation that includes a text to speech synthesizer

  